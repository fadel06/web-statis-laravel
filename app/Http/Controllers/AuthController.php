<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
      return view('register');
    }

    public function handlePost(Request $request){
      // dd($request->all());
      $first = $request['First'];
      $last = $request['Last'];

      return view('welcome', compact('first', 'last'));
    }
}
