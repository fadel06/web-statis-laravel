<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Sign Up</title>
</head>
<body>
	<!-- Bagian atas -->
	<div>
		<h1> Buat Account Baru! </h1>
	</div>

	<!-- Bagian data diri -->
	<div>
		<h3> Sign Up Form </h3>
		<!-- isian -->
		<form action="/welcome" method="post">
			@csrf
			<!-- Nama -->
			<label for="first_name"> First name: </label>
			<br><br>
			<input type="type" name="First" id="first_name">
			<br><br>
			<label for="last_name"> Last name: </label>
			<br><br>
			<input type="type" name="Last" id="last_name">
			<br><br>
			<!-- Gender -->
			<label> Gender: </label>
			<br><br>
			<input type="radio" name="gender"> Male		<br>
			<input type="radio" name="gender"> Female	<br>
			<input type="radio" name="gender"> Other
			<br><br>
			<!-- Kewarganegaraan -->
			<label> Nationality: </label>
			<br><br>
			<select>
				<option>Indonesian</option>
				<option>Singaporean</option>
				<option>Malaysian</option>
				<option>Australian</option>
			</select>
			<br><br>
			<!-- Bahasa -->
			<label> Language Spoken: </label>
			<br><br>
			<input type="checkbox"> Bahasa Indonesia <br>
			<input type="checkbox"> English <br>
			<input type="checkbox"> Other
			<br><br>
			<!-- Bio -->
			<label> Bio: </label>
			<br><br>
			<textarea cols="25" rows="7"></textarea>
			<br>
			<!-- Submit -->
			<p></p>
			<input type="submit" value="Sign Up">
		</form>
	</div>

</body>
</html>
